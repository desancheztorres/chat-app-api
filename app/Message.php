<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['body', 'read', 'conversation_id', 'user_id'];

    public function conversation() {
        return $this->belongsTo(Conversation::class);
    }
}
