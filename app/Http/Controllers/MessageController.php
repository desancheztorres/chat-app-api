<?php

namespace App\Http\Controllers;

use App\Http\Requests\storeMessageRequest;
use App\Http\Resources\MessageResource;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MessageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(storeMessageRequest $request)
    {
        $message = new Message;
        $message->body = $request->body;
        $message->read = false;
        $message->user_id = 1;
        $message->conversation_id = $request->conversation_id;

        $message->save();

        return new MessageResource($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Message  $message
     * @return Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
