<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Http\Requests\StoreConversationRequest;
use App\Http\Resources\ConversationResource;
use App\Message;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $conversations = Conversation::where('user_id', auth()->user()->id)->orWhere('seconde_user_id', auth()->user()->id)->orderBy('updated_at', 'desc')->get();
        $count = count($conversations);

        for($i = 0; $i < $count; $i ++) {
            for($j = $i + 1; $j < $count; $j++) {
                if($conversations[$i]->messages->last()->id < $conversations[$j]->messages->last()->id) {
                    $temp = $conversations[$i];
                    $conversations[$i] = $conversations[$j];
                    $conversations[$j] = $temp;
                }
            }
        }

        return ConversationResource::collection($conversations);

    }

    public function markConversationsAsRead(Request $request) {
        $request->validate([
            'conversation_id' => 'required'
        ]);

        $conversation = Conversation::findOrFail($request->conversation_id);

        foreach ($conversation->messages as $message) {
            $message->update(['read' => true]);
        }

        return response(null, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function store(StoreConversationRequest $request)
    {
        $conversation = Conversation::create([
            'user_id' => auth()->user()->id,
            'seconde_user_id' =>  $request['user_id']
        ]);

        Message::create([
            'user_id' => auth()->user()->id,
            'conversation_id' => $conversation->id,
            'read' => false,
            'body' => $request['message']
        ]);

        return new ConversationResource($conversation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Conversation $conversation
     * @return void
     */
    public function update(Request $request, Conversation $conversation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Conversation $conversation
     * @return void
     */
    public function destroy(Conversation $conversation)
    {
        //
    }
}
