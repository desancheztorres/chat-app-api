<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiAuthController extends Controller
{
    public function login(Request $request) {

        try {
            $http = new \GuzzleHttp\Client;

            $response = $http->post('http://chatapp.test/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => 'Kq4FDW4pXWCVpFshJQ5pSJdZeMstrpA95UJaB6Xw',
                    'username' => $request->email,
                    'password' => $request->password,
                    'scope' => ''
                ],
            ]);

            return $response->getBody();
        } catch(\GuzzleHttp\Exception\BadResponseException $e) {
            return response()->json('errors', $e->getCode());
        }
    }

    public function logout() {
        auth()->user()->tokens->each(function($token) {
            $token->delete();
        });

        return response(null, 201);
    }
}
